<?php get_header(); ?> 
                  <?php /* If this is a category archive */ if (is_category()) { ?>
                    <div class="pagetitle"><?php printf(__('&#8216;%s&#8217; Stories', 'paragrams'), single_cat_title('', false)); ?></div>
                    <?php 
                      $category = get_category( get_query_var('cat') );
                      if ( ! empty( $category ) )
                        echo '<div class="category-feed"><a href="' . get_category_feed_link( $category->cat_ID ) . '" title="' . sprintf( __( 'Subscribe to this category', 'paragrams'), $category->name ) . '" rel="nofollow">' . __( 'Subscribe!', 'paragrams' ) . '</a></div>';
                    ?>
                  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
                    <div class="pagetitle"><?php printf(__('Posts Tagged &#8216;%s&#8217;', 'paragrams'), single_tag_title('', false) ); ?></div>
                  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
                    <div class="pagetitle"><?php printf(_c('Archive for %s|Daily archive page', 'paragrams'), get_the_time(__('F jS, Y', 'paragrams'))); ?></div>
                  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
                    <div class="pagetitle"><?php printf(_c('Archive for %s|Monthly archive page', 'paragrams'), get_the_time(__('F, Y', 'paragrams'))); ?></div>
                  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
                    <div class="pagetitle"><?php printf(_c('Archive for %s|Yearly archive page', 'paragrams'), get_the_time(__('Y', 'paragrams'))); ?></div>
                  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
                    <div class="pagetitle"><?php _e('Author Archive', 'paragrams'); ?></div>
                  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                    <div class="pagetitle"><?php _e('Blog Archives', 'paragrams'); ?></div>
                  <?php } ?>

              <ul class="mcol">
              <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
              	<li class="article" id="post-<?php the_ID(); ?>">

                    	<?php
                    	if ( has_post_thumbnail() ) { ?>
                    	<?php 
                    	$imgsrcparam = array(
						'alt'	=> trim(strip_tags( $post->post_excerpt )),
						'title'	=> trim(strip_tags( $post->post_title )),
						);
                    	$thumbID = get_the_post_thumbnail( $post->ID, 'background', $imgsrcparam ); ?>
                        <div><a href="<?php the_permalink() ?>" class="preview"><?php echo "$thumbID"; ?></a></div>
                    	<?php } ?>


                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <div class="postmetadata">
                        Posted: <?php the_time(__('F jS, Y', 'paragrams')) ?><br />
                        <?php printf(__('Filed under: %s', 'paragrams'), get_the_category_list(', ')); ?>
                    </div>
                </li>

            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>
            
                </ul>

            <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
            <?php endwhile; ?>
            <?php else : ?>
                <h1 id="error"><?php _e("Sorry, but you are looking for something that isn&#8217;t here.", 'paragrams'); ?></h1>
            <?php endif; ?>

            <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
            <?php endwhile; ?>
                <div id="nav">
                    <div id="navleft"><?php next_posts_link(__('Previous page&nbsp;', 'paragrams')) ?></div>
                    <div id="navright"><?php previous_posts_link(__('Next page&nbsp;', 'paragrams')) ?></div>
                </div>
            <?php else : ?>
            <?php endif; ?>
<?php get_footer(); ?>
