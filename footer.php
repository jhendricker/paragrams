        <div id="footer">
        	<div id="copyright">&copy; <?php the_time(__('Y', 'paragrams')) ?> <?php bloginfo('name'); ?>. All images are copyrighted by their respective authors.</div>
            <div id="credits"><?php /*Powered by Wordpress. Designed by <a href="http://wpshower.com/">WPSHOWER</a>*/?></div>
        </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>
