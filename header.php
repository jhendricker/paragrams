<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats please -->

	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1"><![endif]-->
    <!--[if IE 6]>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/ie6.css" type="text/css" media="screen" />
    <![endif]-->
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/ie7.css" type="text/css" media="screen" />
    <![endif]-->
    <!--[if IE 8]>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/ie8.css" type="text/css" media="screen" />
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png" type="image/x-icon" />
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu.css" type="text/css" media="screen" />
 	<?php wp_enqueue_script('jquery'); ?>
 	<?php wp_head(); ?>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/columnizer.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/columnize.js" type="text/javascript"></script>
	<script type='text/javascript' src='<?php echo esc_url( get_template_directory_uri() ); ?>/js/menu.js'></script>

</head>
<body <?php body_class(); ?>>
	<div id="outer">
    	<div id="header">
        	<div id="logo">
        		<?php         		
	        	ob_start();
				ob_implicit_flush(0);
				echo get_option('paragrams_custom_logo'); 
				$my_logo = ob_get_contents();
				ob_end_clean();
        		if (
		        $my_logo == ''
        		): ?>
        		<a href="<?php echo esc_url( home_url() ); ?>/">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>" /></a>
        		<?php else: ?>
        		<a href="<?php echo esc_url( home_url() ); ?>/"><img src="<?php echo get_option('paragrams_custom_logo'); ?>" alt="<?php bloginfo('name'); ?>" /></a>        		
        		<?php endif ?>
        	</div>
            <div id="serv">
            	<div id="search"><?php get_search_form(); ?></div>
                <div id="navicons">
                	<ul>
                    	<li><a id="subscribe" href="https://www.instagram.com/ucfsciences/" title="">Instagram</a></li>
                    	<li><a id="twitter" href="<?php echo get_option('paragrams_twturl'); ?>" title="">Twitter</a></li>
                    	<li><a id="facebook" href="<?php echo get_option('paragrams_fbkurl'); ?>" title="">Facebook</a></li>
                    </ul>
                </div>
                <div id="menu">
				<?php
				    if ( function_exists( 'wp_nav_menu' ) )
				        wp_nav_menu( array( 'theme_location' => 'custom-menu','fallback_cb'=> 'custom_menu','container' => 'ul','menu_id' => 'top-menu', ) );
				    else
				        custom_menu();
				?>
                </div>
            </div>
        </div>
