<?php get_header(); ?> 

<?php 
    // Pagination variable for custom loop
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    // Only grab posts that are in the "News" category and are not marked "Sticky"
    $args = array('category_name' => 'news', 'post__not_in' => get_option( 'sticky_posts' ), 'paged' => $paged  );
    $customHomeQuery = new WP_Query( $args );

    // Only grab "Sticky" posts
    /* 
       $stickyArgs = array('post__in'  => get_option( 'sticky_posts' ), 'paged' => $paged);
       $stickyQuery = new WP_Query ($stickyArgs);    
    */
?>
            <ul class="mcol cat">              
            <?php 
            /*
            if($stickyQuery->have_posts()) : ?><?php while($stickyQuery->have_posts()) : $stickyQuery->the_post(); ?>
                <li class="article">
                
                        <?php
                        if ( has_post_thumbnail() ) { ?>
                        <?php 
                        $imgsrcparam = array(
                        'alt'   => trim(strip_tags( $post->post_excerpt )),
                        'title' => trim(strip_tags( $post->post_title )),
                        );
                        $thumbID = get_the_post_thumbnail( $post->ID, 'background', $imgsrcparam ); ?>
                        <div><a href="<?php the_permalink() ?>" class="preview"><?php echo "$thumbID"; ?></a></div>
                        <?php } ?>
                
                
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <div class="postmetadata">
                        Posted: <?php the_time(__('F jS, Y', 'paragrams')) ?><br />
                        <?php printf(__('Filed under: %s', 'paragrams'), get_the_category_list(', ')); ?>
                    </div>
                </li>
            <?php endwhile; ?> <?php wp_reset_postdata(); ?><?php endif; 
            */
            ?>

            <?php if($customHomeQuery->have_posts()) : ?><?php while($customHomeQuery->have_posts()) : $customHomeQuery->the_post(); ?>
              	<li class="article">
                
                    	<?php
                    	if ( has_post_thumbnail() ) { ?>
                    	<?php 
                    	$imgsrcparam = array(
						'alt'	=> trim(strip_tags( $post->post_excerpt )),
						'title'	=> trim(strip_tags( $post->post_title )),
						);
                    	$thumbID = get_the_post_thumbnail( $post->ID, 'background', $imgsrcparam ); ?>
                        <div><a href="<?php the_permalink() ?>" class="preview"><?php echo "$thumbID"; ?></a></div>
                    	<?php } ?>                
                
                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <div class="postmetadata">
                        Posted: <?php the_time(__('F jS, Y', 'paragrams')) ?><br />
                        <?php printf(__('Filed under: %s', 'paragrams'), get_the_category_list(', ')); ?>
                    </div>
                </li>
            <?php endwhile; ?> <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <?php endif; ?>
            </ul>
        
            <?php if($customHomeQuery->have_posts()) : ?><?php /* while($customHomeQuery->have_posts()) : $customHomeQuery->the_post(); ?>
            <?php endwhile; */ ?>
            <?php else : ?>
            <div id="main">
                <h1><?php _e("Sorry, but you are looking for something that isn&#8217;t here.", 'paragrams'); ?></h1>
            </div>
            <?php endif; ?>
        
            <?php if($customHomeQuery->have_posts()) : ?><?php /* while($customHomeQuery->have_posts()) : $customHomeQuery->the_post(); ?>
            <?php endwhile; */ ?>
                <div id="nav">
                    <div id="navleft"><?php next_posts_link(__('Previous page&nbsp;', 'paragrams')) ?></div>
                    <div id="navright"><?php previous_posts_link(__('Next page&nbsp;', 'paragrams')) ?></div>
                </div>
            <?php else : ?>
            <?php endif; ?>    
        
<?php get_footer(); ?>
